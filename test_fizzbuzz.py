from fizzbuzz import FizzBuzz
import pytest


@pytest.fixture
def fb():
    return FizzBuzz()

def test_length(fb):
    assert len(fb.make_sequence()) == 101

def test_n_buzz(fb):
    n_buzz = sum([1 for e in fb.make_sequence() if e == 'Buzz'])
    assert n_buzz == 14-2

def test_n_fizz(fb):
    n_fizz = sum([1 for e in fb.make_sequence() if e == 'Fizz'])
    assert n_fizz == 27-2

def test_n_fizzbuzz(fb):
    n_fizzbuzz = sum([1 for e in fb.make_sequence() if e == 'FizzBuzz'])
    assert n_fizzbuzz == 6

def test_fizz(fb):
    fizz_idx = [i for i, e in enumerate(fb.make_sequence()) if e == 'Fizz']
    assert fizz_idx[:5] == [6, 9, 12, 18, 24]

def test_buzz(fb):
    buzz_idx = [i for i, e in enumerate(fb.make_sequence()) if e == 'Buzz']
    assert buzz_idx[:5] == [10, 20, 25, 35, 40]

def test_fizzbuzz(fb):
    fizzbuzz_idx = [i for i, e in enumerate(fb.make_sequence()) if e == 'FizzBuzz']
    assert fizzbuzz_idx[:5] == [15, 30, 45, 60, 75]

def test_flamingo(fb):
    fizz_idx = [i for i, e in enumerate(fb.make_sequence()) if e == 'Flamingo']
    assert fizz_idx[:5] == [0, 1, 2, 3, 5]

def test_pink_flamingo(fb):
    pink_flamingo_idx = [i for i, e in enumerate(fb.make_sequence()) if e == 'PinkFlamingo']
    assert len(pink_flamingo_idx) == 0
