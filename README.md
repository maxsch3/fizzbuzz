# FixxBuzz and Pink Flamingos

This program prints the numbers from 0 to 100. But for multiples of three it prints
“Fizz” instead of the number and for multiples of five prints “Buzz”. For numbers which are
multiples of both three and five print “FizzBuzz”. For a member of Fibonacci sequence it prints 
“Flamingo” and “Pink Flamingo” when it is a multiple of 3 and 5 and a member of the Fibonacci sequence.

#Setup

Clone repository

```shell
git clone git@bitbucket.org:maxsch3/fizzbuzz.git
cd fizzbuzz
```

There are no modules required. You just need a vanilla Python 3

# Usage

```shell
python3 fizzbuzz.py
```