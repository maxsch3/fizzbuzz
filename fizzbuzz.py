

class FizzBuzz:

    def __init__(self, n=101):
        self.n = n

    def fib_seq(self):
        a, b = 0, 1
        while a <= self.n:
            yield a
            a, b = b, a + b

    def make_sequence(self):
        l = list(range(self.n))
        l[3::3] = ['Fizz'] * len(l[3::3])
        l[5::5] = ['Buzz'] * len(l[5::5])
        l[15::15] = ['FizzBuzz'] * len(l[15::15])
        fs = list(self.fib_seq())
        for fn in self.fib_seq():
            l[fn] = 'PinkFlamingo' if l[fn] in ['FizzBuzz'] else 'Flamingo'
        return l

    def __str__(self):
        return str(self.make_sequence())

if __name__ == "__main__":
    print(FizzBuzz())